var sourceSheetName = 'links' //the name of the sheet that contains organizations
var formsSheetName = 'Form Links'; //the name of the sheet that is created to contain organization form links

//Creates a new sheet containing organizations and a link to their unique form
function createFormLinks()
{
  //Uses the given sourceSheetName sheet
  var spreadsheet = SpreadsheetApp.getActive();
  var orgsSheet = spreadsheet.getSheetByName(sourceSheetName);
  orgsSheet.activate();
    
  var rowcount = orgsSheet.getLastRow() - 1;
  var orgs = orgsSheet.getRange(2, 1,rowcount - 1).getValues(); //contains the list of organizations in orgs[x][0]
  var formLinksSheet = createSheet(spreadsheet,formsSheetName); //create a new sheet to place links into
  formLinksSheet.activate();
  
  var headers = [['Org Name', 'Form Link']]; //add org name and form link headers to sheet
  formLinksSheet.getRange(1, 1, headers.length, 2).setValues(headers);

  //Create form for each organization
  for(i = 0; i < orgs.length; i++)
  {
    if(orgs[i][0]) //only create form if row field is not empty
    {
      var uniqueForm = createForm(orgs[i][0]); 
	  var formurl = uniqueForm.getPublishedUrl(); //the link to the organization's unique form
      var rowdata = [[orgs[i][0], formurl]];
      formLinksSheet.getRange(i + 2, 1, rowdata.length, 2).setValues(rowdata);
    }
  }
}

//Creates a unique form based off a given organization name and returns it
function createForm(orgName)
{
  //Create form named "orgName's form"
  var form = FormApp.create(orgName + '\'s form');
  
  //add form stuff
  var item = form.addCheckboxItem();
  item.setTitle('What condiments would you like on your hot dog?');
  item.setChoices([
    item.createChoice('Ketchup'),
    item.createChoice('Mustard'),
    item.createChoice('Relish')
  ]);
  
  form.addMultipleChoiceItem()
    .setTitle('Do you prefer cats or dogs?')
    .setChoiceValues(['Cats','Dogs'])
    .showOtherOption(true);
  
  form.addPageBreakItem()
    .setTitle('Getting to know you');
  form.addDateItem()
    .setTitle('When were you born?');
  
  form.addGridItem()
    .setTitle('Rate your interests')
    .setRows(['Cars', 'Computers', 'Celebrities'])
    .setColumns(['Boring', 'So-so', 'Interesting']);
	
  return form;
}

//Creates a new sheet to contain org name and form url, then returns the sheet
function createSheet(spreadsheet, sheetName)
{
  var formLinksSheet = spreadsheet.getSheetByName(formsSheetName);
  
  if(formLinksSheet) //if a form links sheet exists, clear it out
  {
    formLinksSheet.clear();
  } 
  else //insert a sheet otherwise
  {
    formLinksSheet = spreadsheet.insertSheet(formsSheetName);
  }
  
  return formLinksSheet;
}
